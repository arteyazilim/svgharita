var turkiye = document.getElementById("turkiye"),
    iller = document.querySelectorAll("#turkiye g");

turkiye.addEventListener("mousemove", function(e) {
    var target = e.target, plaka, bolge;
    if (!('nodeName' in target) || target.nodeName.toLowerCase() !== 'path') {
        return true;
    }
    target = target.parentNode;
    if (target.parentNode.getAttribute("id") !== "turkiye") {
        plaka = target.getAttribute("data-plakakodu");
        bolge = target.getAttribute("data-bolge");
        target = target.parentNode;
    } else {
        plaka = target.getAttribute("data-plakakodu");
        bolge = target.getAttribute("data-bolge");
    }

    for (var i = 0; i < iller.length; i++) {
        if (iller[ i ].hasAttribute("data-bolge")) {
            if (iller[ i ].getAttribute("data-bolge") == bolge) {
                iller[ i ].setAttribute("class", "highlighted-area");
            } else {
                iller[ i ].removeAttribute("class");
            }
        }
    }
    target.setAttribute("class", "highlighted-city");
    console.log(target.getAttribute("data-iladi"));
});
